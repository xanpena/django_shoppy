from django.test import TestCase
from .models import Product

# Create your tests here.
class ProductTestCase(TestCase):
    def setUp(self):
        Product.object.create(name='Nombre', description='Descripción', category='Categoría', price=10.20, image=NULL)

    
    def test_products_have_category(self):
        producto = Product.objects.get(name='Nombre')

        self.assertEqual(producto.cateogry, "Categoria")