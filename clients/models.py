from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=255, blank=False)
    phone = models.IntegerField(blank=True)
    email = models.EmailField(max_length=255, blank=False)
    address = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)