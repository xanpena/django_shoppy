from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Informe(models.Model):
    title = models.CharField(max_length=255)
    file_type = models.CharField(max_length=4)
