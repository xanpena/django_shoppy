from django.conf.urls import url
from apirest.views import *


urlpatterns = [
    url(r'^informes/$', InformeList.as_view(), name='informes')
]