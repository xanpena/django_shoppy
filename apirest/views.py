from rest_framework import generics
from .models import Informe
from .serializers import InformeSerializer
from django.shortcuts import get_object_or_404

class InformeList(generics.ListCreateAPIView):
    queryset = Informe.objects.all()
    serializer_class = InformeSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk']
        )
        return obj