from .models import Informe
from rest_framework import serializers

class InformeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Informe
        fields = ('id', 'title', 'file_type')